import { TOKEN, API_URL } from "@/constant/credential";
import axios from "axios";

export default class Http {
	static async post(endpoint, body) {
		const r = await axios.post(`${API_URL}/${endpoint}`, { TOKEN, ...body });
		const res = await r.data.result;
		return res;
	}
}
