import { createApp } from "vue";
import App from "./App.vue";
import { Button } from "ant-design-vue";
import "./index.css";
const app = createApp(App);
app.config.productionTip = false;
app.use(Button);
app.mount("#app");
