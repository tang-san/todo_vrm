local cjson = require("cjson")
local jsonEncode = cjson.encode
local jsonDecode = cjson.decode
local mysql = require 'resty.mysql'
local db, err = mysql:new()
if not db then
    ngx.say(jsonEncode({
        message = "Failed to instantiate mysql: ",
        error = err,
        timestamp = ngx.time(),
        code = -1
    }))
    return
end

db:set_timeout(1000)
local ok, err, errcode, sqlstate = db:connect {
    host = "127.0.0.1",
    port = 55000,
    database = "todos",
    user = "root",
    password = "mysqlpw",
    -- host = "127.0.0.1",
    -- port = 3306,
    -- database = "todos",
    -- user = "root",
    -- password = "12345678",
}



if not ok then
    ngx.say(jsonEncode({
        message = "failed to connect ",
        error = err,
        errcode = errcode,
        sqlstate = sqlstate,
        timestamp = ngx.time(),

    }))
    return
end


ngx.log(ngx.DEBUG, "connected to mysql.")



local function strSplit(delim, str)
    local t = {}

    for substr in string.gmatch(str, "[^" .. delim .. "]*") do
        if substr ~= nil and string.len(substr) > 0 then
            table.insert(t, substr)
        end
    end

    return t
end

-- Read body being passed
-- Required for ngx.req.get_body_data()
ngx.req.read_body();
-- Parser for sending JSON back to the client
-- Strip the api/ bit from the request path
local reqPath = ngx.var.uri:gsub("api/", "");
-- Get the request method (POST, GET etc..)
local reqMethod = ngx.var.request_method
-- Parse the body data as JSON
local body = ngx.req.get_body_data() ==
    -- This is like a ternary statement for Lua
    -- It is saying if doesn't exist at least
    -- define as empty object
    nil and {} or cjson.decode(ngx.req.get_body_data());


local Api = {}
Api.__index = Api
-- Declare API not yet responded
Api.responded = false;
-- Function for checking input from client
function Api.endpoint(method, path, callback)
    -- If API not already responded
    if Api.responded == false then
        -- KeyData = params passed in path
        local keyData = {}
        -- If this endpoint has params
        if string.find(path, "<(.-)>")
        then
            -- Split origin and passed path sections
            local splitPath = strSplit("/", path)
            local splitReqPath = strSplit("/", reqPath)
            -- Iterate over splitPath
            for i, k in pairs(splitPath) do
                -- If chunk contains <something>
                if string.find(k, "<(.-)>")
                then
                    -- Add to keyData
                    keyData[string.match(k, "%<(%a+)%>")] = splitReqPath[i]
                    -- Replace matches with default for validation
                    reqPath = string.gsub(reqPath, splitReqPath[i], k)
                end
            end
        end

        -- return false if path doesn't match anything
        if reqPath ~= path
        then
            return false;
        end
        -- return error if method not allowed
        if reqMethod ~= method
        then
            return ngx.say(
                jsonEncode({
                    error = 500,
                    message = "Method " .. reqMethod .. " not allowed"
                })
            )
        end
        if body.TOKEN ~= 'ee8d8728f435fd550f83852aabab5234ce1da528' then
            return ngx.say(
                jsonEncode({
                    message = 'Invalid Token',
                    code = -1,
                    timestamp = ngx.time()
                })
            )
        end

        -- Make sure we don't run this again
        Api.responded = true;

        -- return body if all OK
        body.keyData = keyData
        return callback(body);
    end

    return false;
end

Api.endpoint('POST', '/todo_list',
    function(body)
        local q = "select * from todos.todo"
        local res, err = db:query(q)
        if not res then
            return ngx.say(jsonEncode({
                message = "Failed to query",
                error = err,
                timestamp = ngx.time(),
                code = -1
            }))
        end
        return ngx.say(
            jsonEncode(
                {
                    body = body,
                    timestamp = ngx.time(),
                    result = res
                }
            )
        );
    end
)

Api.endpoint('POST', '/del_todo',
    function(body)
        local q = "delete from todos.todo where id=" .. body.id
        local res, err = db:query(q)
        if not res then
            return ngx.say(jsonEncode({
                message = "Failed to query",
                error = err,
                timestamp = ngx.time(),
                code = -1
            }))
        end
        return ngx.say(
            jsonEncode(
                {
                    code = 1,
                    message = "Success delete ID:" .. body.id
                }
            )
        );
    end
)


Api.endpoint('POST', '/add_todo',
    function(body)
        local text, created_at = body.text, ngx.time()
        local q = "insert into todos.todo (text,created_at) values(\'" .. text .. "\'" .. ',' .. created_at .. ')'
        ngx.log(ngx.DEBUG, q)
        local res, err = db:query(q)
        if not res then
            return ngx.say(jsonEncode({
                message = "Failed to query",
                error = err,
                timestamp = ngx.time(),
                code = -1
            }))
        end
        return ngx.say(
            jsonEncode(
                {
                    body = body,
                    timestamp = ngx.time(),
                    result = res
                }
            )
        );
    end
)
